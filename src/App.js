import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './App.css'
import Footer from './components/Footer/Footer';

import Navbar from './components/navbar/Navbar';
import Homepage from './pages/Homepage/Homepage';
import Underdevelopment from './pages/Underdevelopment/Underdevelopment';


export default class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
    }
  }
  componentDidMount() {

    const header = document.getElementById("header");
    if (header) {
      const sticky = header.offsetTop;
      const scrollCallBack = window.addEventListener("scroll", (srcollEvent) => {
        if (window.pageYOffset > sticky) {
          header.classList.add("sticky");
        } else {
          header.classList.remove("sticky");
        }
      });
      return () => {
        window.removeEventListener("scroll", scrollCallBack);
      };
    }

  }

  render() {
    return (
      <Router>
        <div className="whole">
          <div hidden>
            <Navbar />
          </div>
          <div className="App">
            <Switch>
              <Route path="/" exact component={Underdevelopment} />
              <Route path="/test" component={Homepage} />
            </Switch>
          </div>
          <Footer />
        </div>
      </Router>
    )
  }
}
