import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './Navbar.css'

export default class Navbar extends Component {

    componentDidMount() {

    }

    render() {
        return (
            <header>
                <div id="header" className="header">
                    <div className="header-container">
                        <Link to="/" className="link">
                            <h1>
                                Amnuz Technologies
                            </h1>
                        </Link>
                        <div className="header-items">
                            <Link to="/" className="header-item-option">
                                <p>
                                    Home
                                </p>
                            </Link>
                            <Link to="/" className="header-item-option">
                                <p>
                                    Contact Us
                                </p>
                            </Link>
                            <Link to="/" className="header-item-option">
                                <p>
                                    test
                                </p>
                            </Link>
                            <Link to="/" className="header-item-option">
                                <p>
                                    test
                                </p>
                            </Link>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}
