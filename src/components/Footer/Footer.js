import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './Footer.css'

export default class Footer extends Component {

    constructor(props) {
        super(props)

        this.state = {
        }
    }
    render() {
        return (
            <footer id="footer" >
                <div className="footer-body">
                    <div className="footer-sections">
                        <div>
                            <Link to="/" className="footer-element link footer-logo">
                                <h4>
                                    AMNUZ TECHNOLOGIES
                            </h4>
                            </Link>
                        </div>
                        <div className="footer-element">
                            {/* <img src={this.state.test} alt="email"  /> */}
                            <svg className="footer-svg" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <path d="M0 3v18h24v-18h-24zm21.518 2l-9.518 7.713-9.518-7.713h19.036zm-19.518 14v-11.817l10 8.104 10-8.104v11.817h-20z" />
                            </svg>
                            <div>
                                <h5
                                    onClick={() => {
                                        window.location = "mailto:info@amnuz.com"
                                    }}
                                    className="footer-link">
                                    info@amnuz.com
                            </h5>
                            </div>
                        </div>
                        <div className="footer-element">
                            <svg className="footer-svg" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <path d="M6.176 1.322l2.844-1.322 4.041 7.89-2.724 1.341c-.538 1.259 2.159 6.289 3.297 6.372.09-.058 2.671-1.328 2.671-1.328l4.11 7.932s-2.764 1.354-2.854 1.396c-7.862 3.591-19.103-18.258-11.385-22.281zm1.929 1.274l-1.023.504c-5.294 2.762 4.177 21.185 9.648 18.686l.971-.474-2.271-4.383-1.026.5c-3.163 1.547-8.262-8.219-5.055-9.938l1.007-.497-2.251-4.398z" />
                            </svg>
                            <div>
                                <h5
                                    onClick={() => {
                                        window.location = "tel:+919562127000"
                                    }}
                                    className="footer-link">
                                    +91 956 212 7000
                            </h5>
                            </div>
                        </div>
                    </div>
                    <p className="footer-rights-reserved">
                        © {new Date().getFullYear()} Amnuz Technologies.  All rights reserved.
                    </p>
                    <div>
                    </div>
                </div>
            </footer>
        )
    }
}
