import React, { Component } from 'react'

import './Homepage.css'

import HomeSettings from '../../images/home-settings.svg'
import Business from '../../images/business-interview.svg'
import { Link } from 'react-router-dom'

export default class Homepage extends Component {
    render() {
        return (
            <div>
                <div className="hero">
                    <Link to="/home" className="hero-section link">
                        <div className="hero-left">
                            <img className="hero-image" src={HomeSettings} alt="home" />
                            <h3>Home</h3>
                        </div>
                    </Link>
                    <Link to="/business" className="hero-section link">
                        <div className="hero-right">
                            <img className="hero-image" src={Business} alt="business" />
                            <h3>Business</h3>
                        </div>
                    </Link>
                </div>
                {/* <div className="page-section">
                    <h1>
                        Homepage
                    </h1>
                </div> */}
            </div>
        )
    }
}
