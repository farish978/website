import React, { Component } from 'react'

import './underconstruction.css'

export default class Underdevelopment extends Component {

    constructor(props) {
        super(props)
        this.onMouseEnter = this.onMouseEnter.bind(this);
        this.state = {
            width: "10%"
        }
    }

    onMouseEnter() {
        this.setState({
            width: "100%"
        })
    }

    render() {
        return (
            <div className="Underconstruction-page">
                <div className="construction-hero">
                    <div
                        onMouseEnter={this.onMouseEnter}
                    >

                        <h1
                            // style={{ maxWidth: this.state.width, transitionDuration: "0.5s" }}
                            className="text1">
                            AMNUZ
                        </h1>
                        <h1
                            // style={{ maxWidth: this.state.width, transitionDuration: "0.5s" }}
                            className="text2">
                            TECHOLOGIES
                        </h1>
                    </div>
                </div>
                <div>
                    <p
                        className="construction-message"
                    >
                        We're currently in the process of rebranding.
                </p>
                </div>
            </div>
        )
    }
}
